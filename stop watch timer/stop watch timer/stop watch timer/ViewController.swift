//
//  ViewController.swift
//  stop watch timer
//
//  Created by clicklabs92 on 16/01/15.
//  Copyright (c) 2015 clicklabs92. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var timerLabel: UILabel!
    
    var startTime = NSTimeInterval()
    var miliSecCounter = 00
    var secondCounter = 00
    var minuteCounter = 00
    
    var timer = NSTimer()
    
    func updateTime()
    {
        
        miliSecCounter++
        
        if miliSecCounter == 60{
            secondCounter++
            miliSecCounter = 0
        }
        if  secondCounter == 60  {
            minuteCounter++
            secondCounter = 0
            // miliSecCounter == 00
        }
        
        timerLabel.text = String(minuteCounter) + " :" + String(secondCounter) + ":" + String(miliSecCounter)
        
    }
    //play Button
    @IBAction func play(sender: AnyObject) {
        
        if !timer.valid {
            let aSelector : Selector = "updateTime"
            timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: aSelector, userInfo: nil, repeats: true)
            startTime = NSDate.timeIntervalSinceReferenceDate()
        }
        
    }
    //pause button
    @IBAction func pause(sender: AnyObject) {
        timer.invalidate()
    }
    //reset Button
    @IBAction func reset(sender: AnyObject) {
        timer.invalidate()
        timerLabel.text = "00:00:00"
        miliSecCounter = 0
        secondCounter = 0
        minuteCounter = 0
    }
    
    //split label
    
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    
    
    var islap = false
    var countPress = 0
    
    //split Button Action
    @IBAction func SplitButton(sender: AnyObject) {
        
        //var isLap = true
        
        if countPress == 1 {
            
            label1.hidden = false
            label1.text = timerLabel.text
            // var isLap = false
            println("it Works with label1")
            label2.hidden = true
        }
        if countPress == 2 {
            label1.hidden = false
            label2.hidden = false
            label2.text = timerLabel.text
            // var isLap = false
            println("it Works with label 2")
        }
        if countPress == 3 {
            label1.hidden = false
            label2.hidden = false
            label3.hidden = false
            label3.text = timerLabel.text
            // var isLap = false
            println("it Works with label 2")
        }
        countPress++
        if countPress == 4 {
            countPress = 1
        }
        
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}


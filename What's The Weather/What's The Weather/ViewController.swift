//
//  ViewController.swift
//  What's The Weather
//
//  Created by clicklabs92 on 22/01/15.
//  Copyright (c) 2015 clicklabs92. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

 //text field variable for entering city name
 @IBOutlet weak var city: UITextField!
    
//label field to show the temperature
 @IBOutlet weak var message: UILabel!
    
//code for button(find result)
 @IBAction func buttonPressed(sender: AnyObject) {
        
        self.view.endEditing(true)
        
       //set url from where the message will retrieve
        var urlString = "http://www.weather-forecast.com/locations/" + city.text.stringByReplacingOccurrencesOfString(" ", withString: "") + "/forecasts/latest"
           var url = NSURL(string: urlString)
        
          //access the web component
           let task = NSURLSession.sharedSession().dataTaskWithURL(url!) {(data, response, error) in
               var urlContent = NSString(data: data, encoding: NSUTF8StringEncoding)
            
       //set condition to prevent the crash
       if NSString (string: urlContent!).containsString("<span class=\"phrase\">") {
            //break whole message into two parts..
               var contentArray = urlContent!.componentsSeparatedByString("<span class=\"phrase\">")
            
            //take part 1  and break into two parts again
                var newcontentArray = contentArray[1].componentsSeparatedByString("</span>")
            
            //take part 0 and replace &deg with "º"
               var weatherForcast = newcontentArray[0].stringByReplacingOccurrencesOfString("&deg;", withString: "º")
        
        
             //if city found then display the weather
                dispatch_async(dispatch_get_main_queue()) {
                   self.message.hidden = false
                   self.message.text = weatherForcast

                } //end of dispatch
                    
        } //end of if statement
        
        else {
          //if city not found then display this message
                dispatch_async(dispatch_get_main_queue()) {
                    self.message.text = "Could Not Find The City, Please Try Again..."
                        } //end of dispatch
        
            } //end of else statement
        
    }//end of NSURLSession condition
    
                         task.resume()
    
}

       //for hiding the keyboard when click on screen
         override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
              self.view.endEditing(true)
    
       }
    
    //hide the keyboard when return pressed
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        city.resignFirstResponder()
        return true
    }
    
    override func viewDidLoad() {
        
        
                super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    
    
    
  
} //end of class





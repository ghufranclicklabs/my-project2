//
//  FirstViewController.swift
//  To Do List
//
//  Created by clicklabs92 on 20/01/15.
//  Copyright (c) 2015 clicklabs92. All rights reserved.
//

import UIKit

//create array of type string and initialize to empty 
       var toDoItems:[String] = []

//first view cotroller where added item will appear
    class FirstViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

   //refer to table view
    @IBOutlet weak var taskstable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //counts the number of rows and return it
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return toDoItems.count
    }
    
    
    //create var cell and convert all rows in cells and return all cells
     func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        var cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        
        //index path used for number of index of the cell
        cell.textLabel?.text = toDoItems[indexPath.row]
           return cell
        }
    
    // checking and stored the item in array
   override func viewWillAppear(animated: Bool) {
      if  var storedtoDoItems: AnyObject =  NSUserDefaults.standardUserDefaults().objectForKey("toDoItems") {
            
          // an empty array to store the object
            
            toDoItems = []
        for var i = 0; i < storedtoDoItems.count; ++i {
               toDoItems.append(storedtoDoItems[i] as NSString)
              }
        }
      
        //reload the table
        taskstable.reloadData()
}
    
    
    
    
    //function used for deleting the cell information on slide...
    
    func tableView(tableView: UITableView!, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath!) {
        if (editingStyle == UITableViewCellEditingStyle.Delete) {
            
            // handle delete (by removing the data from your array and updating the tableview)
                toDoItems.removeAtIndex(indexPath!.row)
                  tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
          
            }
    }
    


   //edit the table in view table

     func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath){
        if (editingStyle == UITableViewCellEditingStyle.Delete){
            
            toDoItems.removeAtIndex(indexPath.row)
        }//end of if statement
        
        }
        
        
    //when cell is selected then an alert is popedUp to show message that you have selected perticular cell...
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        println("It works!")
        let alert = UIAlertController(title: "Item selected", message: "You selected item \(indexPath.row)", preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: "OK",
            style: UIAlertActionStyle.Default,
            handler: {
                (alert: UIAlertAction!) in println("An alert of type \(alert.style.hashValue) was tapped!")
        }))
        
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    

}

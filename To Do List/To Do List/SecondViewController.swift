//
//  SecondViewController.swift
//  To Do List
//
//  Created by clicklabs92 on 20/01/15.
//  Copyright (c) 2015 clicklabs92. All rights reserved.
//

import UIKit

//second view controller where add the item

 class SecondViewController: UIViewController,UITextFieldDelegate {
    
   //text field variable....
    @IBOutlet weak var toDoitem: UITextField!
    
    
    //add item in the view table....
    @IBAction func addItem(sender: AnyObject) {
    
    //add the new item in the array 
    toDoItems.append(toDoitem.text)
    
      //stored item in table permanently
    let fixedtoDoItems = toDoItems
         NSUserDefaults.standardUserDefaults().setObject(fixedtoDoItems, forKey: "toDoItems")
         NSUserDefaults.standardUserDefaults().synchronize()
    
    //hiding the keyboard
    self.view.endEditing(true)
    
    
  }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //return button click then keyboard hide
    func textFieldShouldReturn(textField: UITextField) -> Bool{
        textField.resignFirstResponder()
          return true
    }
    
    //when click on text box keyboard hide
     override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        self.view.endEditing(true)
    }


}

